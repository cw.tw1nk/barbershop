import '../styles/style.scss';
import '../styles/normilize.scss';

import $ from 'jquery'

$(document).mouseup((e) => {
	var modal = $('.modal-body');
	if ($('.icon-modal-close').is(e.target)){
		$('.modal').hide();
	}
	if (!modal.is(e.target) && modal.has(e.target).length===0){
		$('.modal').hide();
	}
});
$('#open-modal').click(function () {
	return $($(this).attr('href')).show();
});
