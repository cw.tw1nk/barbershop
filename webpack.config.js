const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const webpack = require("webpack");
const { extendDefaultPlugins } = require("svgo");

const config = {
	entry: {
		app: './src/js/app.js',
	},
	output: {
		filename: 'js/[name].js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'inline-source-map',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader'],
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				type: "asset",
			},
			{
				test: /\.(woff|woff2|ttf|eot)$/,
				loader: 'file-loader'
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				loader:'file-loader'
			},
		]
	},
	plugins: [
		new BrowserSyncPlugin({
				host: 'localhost',
				port: 8000,
				proxy: 'http://localhost:8080/',
				ghostMode: {
					clicks: false,
					location: false,
					forms: false,
					scroll: false,
				},
				injectChanges: true,
				logFileChanges: true,
				logLevel: 'debug',
				logPrefix: 'wepback',
				notify: true,
				reloadDelay: 0,
			},
			{
				reload: false
			}
		),
		// new BrowserSyncPlugin({
		// 	// host: 'localhost',
		// 	// port: 3000,
		// 	// proxy: 'http://localhost:3100/',
		// 	// server: {
		// 	// 	baseDir: ['dist']
		// 	// },
		// 	index: 'index.html',
		// 	files: ['**/*'],
		// 	ghostMode: {
		// 		clicks: false,
		// 		location: false,
		// 		forms: false,
		// 		scroll: false,
		// 	},
		// 	injectChanges: true,
		// 	logFileChanges: true,
		// 	logLevel: 'debug',
		// 	logPrefix: 'wepback',
		// 	notify: true,
		// 	reloadDelay: 0,
		// }, {
		// 	reload: false
		// }),
		new HtmlWebpackPlugin({
			inject: true,
			hash: false,
			filename: 'index.html',
			template: path.resolve(__dirname, 'src', 'index.html')
		}),
		new MiniCssExtractPlugin({
			filename: 'css/[name].css',
		}),
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, 'src', 'images'),
				to: path.resolve(__dirname, 'dist', 'images'),
				toType: 'dir',
			},
		]),
		new ImageMinimizerPlugin({
			minimizerOptions: {
				// Lossless optimization with custom option
				// Feel free to experiment with options for better result for you
				plugins: [
					["gifsicle", {interlaced: true}],
					["jpegtran", {progressive: true}],
					["optipng", {optimizationLevel: 5}],
					// Svgo configuration here https://github.com/svg/svgo#configuration
					[
						"svgo",
						{
							plugins: extendDefaultPlugins([
								{
									name: "removeViewBox",
									active: false,
								},
								{
									name: "addAttributesToSVGElement",
									params: {
										attributes: [{xmlns: "http://www.w3.org/2000/svg"}],
									},
								},
							]),
						},
					],
				],
			},
		}),
	]

};

module.exports = config;
